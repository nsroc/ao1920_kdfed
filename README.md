# Frontend Development

Dit document behandelt de volgende onderdelen:

1. Hoe clone ik deze git-repo?
2. Hoe zet ik mijn eigen git-repo op?
3. Hoe voeg ik een docent toe ?
4. Hoe ga ik te werk ?
5. Extra Guidelines

### Benodigdheden

 * Bitbucket account
 * Git geinstalleerd. I.a.w. je moet het git-commando kunnen uitvoeren in de terminal.

---


## 1. Hoe clone ik deze gitrepo ?
Deze git-repo is zo gemaakt om je een handige houvast te geven om alles in te leveren. Deze kun je dan ook clonen en 
vervolgens op je eigen git-omgeving plaatsen. 

Ga naar de plek waar je dit project wilt opslaan en open vervolgens de command-prompt en voer het volgende commando
uit:

```git clone https://nsroc@bitbucket.org/nsroc/ao1920_kdfed.git```

## 2. Hoe zet ik mijn eigen git-repo op?
 
### Stap 2.1: Bitbucket Repo aanmaken

 * Log in op je Bitbucket account en maak een nieuwe repository aan:
    * ![an Image](img/readme/createRepo.png)
    * Geef deze repo als naam: ao1920_STUDENTENNUMMER_fed
    * Access level: This is a private repository
    * Include a README: no
    * Version control system: Git
 * Druk op `Create repository`.
 
### Stap 2.2: Remote toevoegen en pushen
Je hebt nu een _remote_ git-repository aangemaakt. Deze moet gekoppeld worden aan de lokale. Deze lokale bevat als het
goed is al de eerder gekloonde repo.

 * Ga lokaal naar de map waar de repo gekloond is;
 * Voer de volgende commando's uit:
    * ```git remote rename origin upstream```
    * ```git remote add origin https://nsroc@bitbucket.org/nsroc/ao1920_000000_fed.git```
        * Pas de link aan naar het adres van jouw eigen repo!

Nu hebben we de lokale repo gekoppeld met de repo op Bitbucket. Nu gaan we de code die lokaal staat pushen:

 * Ga naar de map waar de repo gekloond is;
    * ```git push -u origin master```

Je hebt nu zowel lokaal als extern dezelfde inhoud staan en je bent klaar om aan de slag te gaan met de opdrachten. Deze
opdrachten moeten echter ook nagekeken worden. Daarvoor moet je een docent toevoegen. In dit geval Nick Stuivenberg. Dat
doen je in het volgende hoofdstuk.

## 3. Hoe voeg ik een docent toe?
Je voegt het account `nsroc` met het e-mailadres `n.stuivenberg@rocmn.nl` toe  aan het hierboven aangemaakte project.
Dat doen we in bitbucket:

 * Ga naar bitbucket.org
    * Selecteer de betreffende repo;
    * Druk op `settings`;
    * Druk op `User and group access`;
    * Voeg onder users de bovengenoemde gebruiker toe. Deze gebruiker heeft niet meer dan leesrechten nodig.
    * Doe dit gelijk! Je kunt mij niet als reviewer toevoegen tot en met ik dit geaccepteerd heb.

## 4. Hoe ga ik te werk?
Je hebt nu een repo aangemaakt, waar ook de docent bij kan. Dat betekent echter niet dat je alles maar op de
master-branch kunt gaan pushen. We willen dat je bij dit vak gebruik gaat maken van `branches`. Dat houdt in dat je voor
elke opdracht een branch aanmaakt en daar jouw opdracht naartoe pusht. Wanneer de opdracht klaar is, open je een
`pull request`. Voor deze pull request nodig je weer de docent uit. Op deze manier kan de docent de code `reviewen` en
eventueel commentaar geven. Wanneer de opdracht goed is, wordt de `pull request` goedgekeurd. Dat betekent dat de
opdracht voldoende gemaakt is. Nadat de pull request goedgekeurd is, moet deze ook gemergd worden met de master. Het
makkelijkste is om dit te doen in Bitbucket. Hiermee garandeer je dat de `master-branch` de meest up-to-date code heeft.
Dat is handig wanneer je voor de volgende opdracht weer van de master gaat branchen. 

We begrijpen wel dat dit allemaal best veel werk is -en misschien nog wel onduidelijk ook. Daarom staat het hieronder
stapsgewijs met screenshots uitgelegd. Deze manier zorgt er echter wel voor dat je steeds netter met git gaat werken. 
Iets wat je ook goed kunt gebruiken bij de projectopdrachten!

### 4.1 Stapsgewijs

Deze stappen maken gebruik van bitbucket en de commandline. Het is ook mogelijk om de stappen geheel via de command line
te doen of gebruik te maken van bijvoorbeeld PhpStorm, SourceTree of GitKraken.

### 4.2 Branch aanmaken

 * Ga naar bitbucket.org
 * Ga naar je repo.
 * Ga naar Branches en druk op `Create branch`.
    * ![](img/readme/createBranch.png)
    * _Type_: `Other`
    * _From branch_ : `master`
    * _Branch name_ : `opdracht1`
    
Je hebt nu in de `remote` repo een branch aangemaakt. Deze willen we nu lokaal gaan aanpassen. Dat wordt hieronder
uitgelegd.

### 4.3 Van Branch wisselen
Je hebt nu via bitbucket.org een branch aangemaakt. Nu moeten we lokaal in die branch gaan werken. Dus we moeten
wisselen van de `master` naar de `opdracht1` branch.

 * Ga naar de project repo op je schijf.
 * Open de terminal.
 * `git fetch`
 * `git switch opdracht1`
    * Het volgende resultaat zou je moeten krijgen: `Switched to branch 'opdracht1'`

Je hebt nu een aparte branch aangemaakt en ben daar _lokaal_ naartoe gewisseld. Als je nu een aanpassing doet gebeurt
dat alleen in de lokale `opdracht1` branch.

### 4.4 Branch pushen en pull request maken

 * Doe een aanpassing in het README.md bestand met je favoriete editor.
 * Doe een commit.
    * `git commit -m "Kleine aanpassing readme opdracht 1"`
    * `git add BESTANDSNAAM VAN BESTAND DAT JE AANGEPAST HEBT`
 * Doe een push:
    * `git push -u origin`

We gaan er nu vanuit dat de opdracht af is. We gaan daarom een pull request maken om deze te laten goedkeuren.

 * Ga naar de repository op bitbucket.org.
 * Ga naar `Pull requests` en druk op `Create pull request`
    * Selecteer de branch die je wilt mergen `opdracht1`
    * Selecteer de branch waarheen je wilt mergen `master`
    * Schrijf een korte titel die de pull request beschrijft
    * Schrijf een beschrijving.
    * *Voeg* de docent (nsroc of n.stuivenberg@rocmn.nl) toe als reviewer.
    * Druk op `Create pull request`
    * ![](img/readme/createPullRequest.png)

Je hebt nu een pull-request gemaakt met de docent als reviewer. Hier krijg je commentaar op, of deze wordt goedgekeurd.
Na goedkeuring kun je de pull request mergen naar de master branch. Zodat je volgende branch (vanaf de master) weer de 
meeste up-to-date code heeft.

### 4.5 Pull request sluiten en mergen
Je docent heeft de pull request goedgekeurd. Deze kun je nu weer terugmergen. Het zou er dan ongeveer zo moeten uitzien:

![](img/readme/mergePullRequest.png)

 * Rechtsboven staat de `Approve` op 1.
 * Er is commentaar door de docent toegevoegd.
 
 Je kunt nu de code mergen door op `Merge` te drukken. Dan krijg je het volgende scherm:
 
 ![](img/readme/confirmMerge.png).
 
 * Druk hier op `Merge`.
 * `Close source branch` mag hier aangevinkt worden, wanneer er niet meer aan de branch gewerkt gaat worden.

Jouw code is nu in de `remote` repo gemergd. Dat wil zeggen dat de twee branches `master` en `opdracht1` zijn
samengevoegd op de `master` branch. Dit is alleen in de `remote` repo gebeurd. Dit moet ook nog lokaal gebeuren!

### 4.6 Lokaal de meest up to date code krijgen.
In de vorige stap heb je in de `remote` repo de code gemergd. Lokaal heeft de `master` branch echter nog de oude code.
We gaan in deze stap de `master` branch weer up to date maken, opdat we hier weer vanaf kunnen mergen.

 * Ga naar de project repo op je schijf.
 * Open de terminal.
 * Check op welke branch je bent:
    * `git branch` . Dit zou `opdracht1` moeten teruggeven.
 * Haal de nieuwe data op van de `remote` repo.
    * `git fetch`.
 * Switch lokaal naar de master-branch
    * `git switch master`
 * `pull` de meeste up-to-date code
    * `git pull origin master`
 * Voor de volgende opdracht kun je nu weer bij de stap `branch aanmaken` kijken.
 
## 5. Guidelines
 
 Als het goed is heb je nu genoeg handvaten om te kunnen werken met git. Hieronder staan nog een paar punten om op te
 letten:
 
 * Verwijder gesloten en voltooide branches _niet_!
 * Neem de master-branch altijd als startpunt.
 * Wees niet bang om vragen te stellen.
 * Maak voor elke opdracht een nieuwe folder aan.
 * Tekstuele opdrachten hoeven niet in een Word-document ingeleverd te worden. Het mag ook in _Markdown_.
 (zoals dit document.)
    * Ook deze opdrachten moeten via git ingeleverd worden!
 * Schrijf bruikbare _commit-messages_. Op deze manier kan de docent & jijzelf zien wat er in de commit aangepast is in
 de code.
 * Zorg dat al je werk _pusht_ -dus niet alleen gecommit- wordt! Commit en push ook als je maar een regel code
 hebt aangepast of toegevoegd. De stelregel is: Hoe kleiner de commits, hoe beter!
 * Als je met meerdere  mensen op één git-repo werkt, doe altijd een git pull voordat je begint met werken of voordat je
 vanaf de master gaat branchen.
 