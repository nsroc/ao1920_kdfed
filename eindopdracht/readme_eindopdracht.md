# Eindopdracht
Gebruik deze folder om de bestanden van de eindopdracht in te plaatsen

## Eisen
 * De lay-out moet ontwikkeld zijn met CSS-grids of Bootstrap
 * SEO moet toegepast zijn
 * UX en optimalisatie moet toegepast zijn
 * Je kunt maximaal twee bonuspunten verdienen met het gebruik van Sass (zelfstudie)